import { assign, createMachine } from "xstate";

export const userOnboardingStateMachine = createMachine(
  {
    id: "userOnboardingStateMachine",
    initial: "profileSetup",
    schema: {
      services: {} as {
        /**
         * Handler for storing profile data, called when we've received the profile data from the user.
         */
        storeProfileData: {
          data: void;
        };
        createWorkspace: {
          data: void;
        };
        onOnboardingCompleteHandler: {
          data: void;
        };
      },
      context: {} as {
        firstName: string;
        lastName: string;
        workspaceName: string;
      },
      events: {} as
        | {
            type: "STORE_PROFILE_DATA";
            value: {
              firstName: string;
              lastName: string;
            };
          }
        | {
            type: "CREATE_WORKSPACE";
            value: {
              workspaceName: string;
            };
          }
        | {
            type: "RETURN_TO_PROFILE_SETUP";
          },
    },
    context: {
      firstName: "",
      lastName: "",
      workspaceName: "",
    },
    states: {
      profileSetup: {
        initial: "waitingForInput",
        states: {
          waitingForInput: {
            on: {
              STORE_PROFILE_DATA: {
                target: "storingProfileData",
                actions: ["addProfileDataToContext"],
              },
            },
          },
          storingProfileData: {
            invoke: {
              id: "storeProfileData",
              src: "storeProfileData",
              onDone: {
                target: "#userOnboardingStateMachine.workspaceCreation",
              },
              onError: {
                target: "waitingForInput",
                actions: ["onErrorStoringProfileData"],
              },
            },
          },
        },
      },
      workspaceCreation: {
        initial: "waitingForInput",
        states: {
          waitingForInput: {
            on: {
              CREATE_WORKSPACE: {
                target: "creatingWorkspace",
                actions: ["addWorkspaceNameToContext"],
              },
              RETURN_TO_PROFILE_SETUP: {
                target: "#userOnboardingStateMachine.profileSetup",
              },
            },
          },
          creatingWorkspace: {
            invoke: {
              id: "createWorkspace",
              src: "createWorkspace",
              onDone: {
                target: "#userOnboardingStateMachine.onboardingComplete",
              },
              onError: {
                target: "waitingForInput",
                actions: ["onErrorCreatingWorkspace"],
              },
            },
          },
        },
      },
      onboardingComplete: {
        type: "final",
      },
    },
  },
  {
    actions: {
      addProfileDataToContext: assign({
        firstName: (ctx, evt) =>
          evt.type === "STORE_PROFILE_DATA"
            ? evt.value.firstName
            : "first name",
        lastName: (ctx, evt) =>
          evt.type === "STORE_PROFILE_DATA" ? evt.value.lastName : "last name",
      }),
      addWorkspaceNameToContext: assign({
        workspaceName: (ctx, evt) =>
          evt.type === "CREATE_WORKSPACE"
            ? evt.value.workspaceName
            : "workspace name",
      }),
    },
  },
);
