import OnboardingWizard from "./OnboardingWizard";

const ServerWrapper = async () => {
  // We could fetch the initial config related to the user here
  async function getInitialContext() {
    // Do something with the data
    return {
      firstName: "John",
      lastName: "Doe",
      workspaceName: "My Workspace",
    };
  }

  const initialContext = await getInitialContext();

  return <OnboardingWizard initialContext={initialContext} />;
};

export default ServerWrapper;
