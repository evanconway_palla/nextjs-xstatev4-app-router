"use client";
import { useMachine } from "@xstate/react";
import { userOnboardingStateMachine } from "@/machines/onboarding-state-machine";

interface Props {
  initialContext?: {
    firstName: string;
    lastName: string;
    workspaceName: string;
  };
  onStoreProfileData?: (data: {
    firstName: string;
    lastName: string;
  }) => Promise<void>;
  onCreateWorkspace?: (data: { workspaceName: string }) => Promise<void>;
}

const OnboardingWizard = (props: Props) => {
  const [state, send] = useMachine(userOnboardingStateMachine, {
    context: props.initialContext,
    services: {
      storeProfileData: async (ctx, evt) => {
        if (props.onStoreProfileData) {
          await props.onStoreProfileData(
            evt.type === "STORE_PROFILE_DATA"
              ? evt.value
              : { firstName: "first name", lastName: "last name" },
          );
        }
        // Store the profile data in your database
      },
      createWorkspace: async (ctx, evt) => {
        if (props.onCreateWorkspace) {
          await props.onCreateWorkspace(
            evt.type === "CREATE_WORKSPACE"
              ? evt.value
              : { workspaceName: "workspace name" },
          );
        }
      },
    },
    actions: {
      onErrorStoringProfileData: (ctx, evt) => {
        // Show Error Toast
      },
      onErrorCreatingWorkspace: (ctx, evt) => {
        // Show Error Toast
      },
    },
  });

  return (
    <div>
      {/* Profile Setup State */}
      {state.matches("profileSetup.waitingForInput") && (
        <div>
          <h2>Profile Setup</h2>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              const formData = new FormData(e.currentTarget);
              const firstName = formData.get("firstName") as string;
              const lastName = formData.get("lastName") as string;
              send({
                type: "STORE_PROFILE_DATA",
                value: {
                  firstName: firstName,
                  lastName: lastName,
                },
              });
            }}
          >
            <input type="text" name="firstName" placeholder="First Name" />
            <input type="text" name="lastName" placeholder="Last Name" />
            <button type="submit">Next</button>
          </form>
        </div>
      )}

      {/* Workspace Creation State */}
      {state.matches("workspaceCreation.waitingForInput") && (
        <div>
          <h2>Create a Workspace</h2>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              const formData = new FormData(e.currentTarget);
              const workspaceName = formData.get("workspaceName") as string;
              send({
                type: "CREATE_WORKSPACE",
                value: {
                  workspaceName: workspaceName,
                },
              });
            }}
          >
            <input
              type="text"
              name="workspaceName"
              placeholder="Workspace Name"
            />
            <button type="submit">Create</button>
            <button
              type="button"
              onClick={() => send({ type: "RETURN_TO_PROFILE_SETUP" })}
            >
              Back
            </button>
          </form>
        </div>
      )}

      {/* Onboarding Complete State */}
      {state.matches("onboardingComplete") && (
        <div>
          <h2>Onboarding Complete</h2>
          <p>
            Welcome, {state.context.firstName} {state.context.lastName}!
          </p>
        </div>
      )}
    </div>
  );
};

export default OnboardingWizard;
