import ServerWrapper from "@/components/ServerWrapper";

export default function Home() {
  return (
    <div>
      <ServerWrapper />
    </div>
  );
}
